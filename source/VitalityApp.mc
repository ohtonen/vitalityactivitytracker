using Toybox.Application as App;
using Toybox.System;

class VitalityApp extends App.AppBase {

    var vitalityView;

	function initialize() 
	{
		AppBase.initialize();
	}

    //! onStart() is called on application start up
    function onStart(state) {
    }

    //! onStop() is called when your application is exiting 
    function onStop(state) {
        vitalityView.stopRecording();
    }

    //! Return the initial view of your application here
    function getInitialView() {
        vitalityView = new VitalityView();
        return [ vitalityView, new BaseInputDelegate() ];
    }

}